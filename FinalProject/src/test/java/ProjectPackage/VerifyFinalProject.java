package ProjectPackage;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class VerifyFinalProject {
    FinalProjectWebdriver finalProjectWebdriver = new FinalProjectWebdriver();
    FinalProject finalProject = PageFactory.initElements(finalProjectWebdriver.webDriver, FinalProject.class);
    Actions actions = new Actions(finalProjectWebdriver.webDriver);
    WebDriverWait webDriverWait = new WebDriverWait(finalProjectWebdriver.webDriver, 20);

    @Test
    public void firstTask() {
        try {
            finalProject.clickDemoTestingSite(actions);
            int actualNumber = 6;
            Assert.assertEquals(finalProject.firstColumnSize(), actualNumber);
            Assert.assertEquals(finalProject.secondColumnSize(), actualNumber);
            Assert.assertEquals(finalProject.thirdColumnSize(), actualNumber);
        } finally {
            finalProjectWebdriver.webDriver.close();
        }
    }

    @Test
    public void secondTask() {
        try {
            finalProject.clickDatePicker(actions);
            finalProjectWebdriver.webDriver.switchTo().frame(finalProject.iframeTask2);
            String expected = finalProject.putDate();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
            Date date = simpleDateFormat.parse(expected);
            Assert.assertTrue(expected.equals(simpleDateFormat.format(date)));
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            finalProjectWebdriver.webDriver.close();
        }
    }

    @Test
    public void thirdTask() {
        try {
            finalProject.clickProgressBar(actions);
            finalProjectWebdriver.webDriver.switchTo().frame(finalProject.iframeTask3);
            String expected = finalProject.download(webDriverWait);
            String actual = "Complete!";
            Assert.assertEquals(expected, actual);
        } finally {
            finalProjectWebdriver.webDriver.close();
        }
    }
}