package ProjectPackage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class FinalProject {
    WebDriver webDriver;

    @FindBy(xpath = "//div[@id='menu']/ul/li[4]")
    WebElement testersHub;

    @FindBy(xpath = "//div[@id='menu']/ul/li[4]/div/ul/li/a")
    WebElement demoTestingSite;

    @FindBy(xpath = "//div[@class='price_column '][1]/ul/li[@class='price_footer']")
    List<WebElement> firstStep;

    @FindBy(xpath = "//div[@class='price_column '][2]/ul/li[@class='price_footer']")
    List<WebElement> secondStep;

    @FindBy(xpath = "//div[@class='price_column '][3]/ul/li[@class='price_footer']")
    List<WebElement> thirdStep;

    @FindBy(xpath = "//div[@id='menu']/ul/li[4]/div/ul/li/div/ul/li[@id='menu-item-2827']")
    WebElement datePicker;

    @FindBy(css = "#post-2661 > div.twelve.columns > div > div > div.single_tab_div.resp-tab-content.resp-tab-content-active > p > iframe")
    WebElement iframeTask2;

    @FindBy(xpath = "//input[@id='datepicker']")
    WebElement datePickerClick;

    @FindBy(xpath = "//div[@id='ui-datepicker-div']/div/a[2]")
    WebElement month;

    @FindBy(xpath = "//div[@id='ui-datepicker-div']/table/tbody/tr[1]/td[2]")
    WebElement day;

    @FindBy(xpath = "//div[@id='menu']/ul/li[4]/div/ul/li/div/ul/li[@id='menu-item-2832']")
    WebElement progressBar;

    @FindBy(css = "#post-2671 > div.twelve.columns > div > div > div.single_tab_div.resp-tab-content.resp-tab-content-active > p > iframe")
    WebElement iframeTask3;

    @FindBy(xpath = "//button[@id='downloadButton']")
    WebElement clickToDownload;

    By waitUntilAppears = By.xpath("//div[@class='ui-dialog-buttonset']/button");

    @FindBy(xpath = "//div[@id='dialog']/div")
    WebElement complete;

    public void clickDemoTestingSite(Actions actions) {
        actions.moveToElement(testersHub).moveToElement(demoTestingSite).click().build().perform();
    }

    public int firstColumnSize() {
        return firstStep.size();
    }

    public int secondColumnSize() {
        return secondStep.size();
    }

    public int thirdColumnSize() {
        return thirdStep.size();
    }

    public void clickDatePicker(Actions actions) {
        actions.moveToElement(testersHub).moveToElement(demoTestingSite).moveToElement(datePicker).click().build().perform();
    }

    public String putDate() {
        datePickerClick.click();
        month.click();
        day.click();
        return datePickerClick.getAttribute("value");
    }

    public void clickProgressBar(Actions actions) {
        actions.moveToElement(testersHub).moveToElement(demoTestingSite).moveToElement(progressBar).click().build().perform();
    }

    public String download(WebDriverWait webDriverWait) {
        clickToDownload.click();
        webDriverWait.until(ExpectedConditions.textToBe(waitUntilAppears, "Close"));
        return complete.getText();
    }
}