import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Test1 {
    @Test
    void checkOnExpectedPage() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Hamida\\Desktop\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        try {
            webDriver.get(" https://www.google.com");
            WebElement searchField = webDriver.findElement(By.cssSelector("body > div.L3eUgb > div.o3j99.ikrT4e.om7nvf > form > div:nth-child(1) > div.A8SBwf > div.RNNXgb > div > div.a4bIc > input"));
            searchField.sendKeys("Selenium");
            WebElement searchButton = webDriver.findElement(By.cssSelector("body > div.L3eUgb > div.o3j99.ikrT4e.om7nvf > form > div:nth-child(1) > div.A8SBwf > div.FPdoLc.tfB0Bf > center > input.gNO89b"));
            searchButton.click();
            WebElement seleniumDev = webDriver.findElement(By.cssSelector("#rso > div:nth-child(1) > div > div > div.yuRUbf > a > h3"));
            seleniumDev.click();
            webDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
            WebElement check = webDriver.findElement(By.cssSelector("body > section.hero.homepage > h1:nth-child(1)"));
            System.out.println(check.getText());
            Assertions.assertEquals(check.getText(), "Selenium automates browsers. That's it!");
        } finally {
            webDriver.close();
        }
    }

    @Test
    void verifyLoggedIn() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Hamida\\Desktop\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        try {
            webDriver.get("https://mail.ru/");
            webDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
            WebElement email = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.email-container.svelte-1eyrl7y > div.email-input-container.svelte-1eyrl7y > input"));
            email.sendKeys("hamida-ibatech");
            WebElement next = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.button.svelte-1eyrl7y"));
            next.click();
            WebElement password = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.password-input-container.svelte-1eyrl7y > input"));
            password.sendKeys("ibatech123");
            WebElement submit = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.second-button.svelte-1eyrl7y"));
            submit.click();
            WebElement check = webDriver.findElement(By.cssSelector("#PH_user-email"));
            System.out.println(check.getText());
            Assertions.assertEquals(check.getText(), "hamida-ibatech@mail.ru");
        } finally {
            webDriver.close();
        }
    }

    @Test
    void checkErrorMessage() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Hamida\\Desktop\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        try {
            webDriver.get("https://mail.ru/");
            webDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
            WebElement email = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.email-container.svelte-1eyrl7y > div.email-input-container.svelte-1eyrl7y > input"));
            email.sendKeys("hamida-ibatech");
            WebElement next = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.button.svelte-1eyrl7y"));
            next.click();
            WebElement password = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.password-input-container.svelte-1eyrl7y > input"));
            password.sendKeys("ibatech12");
            WebElement submit = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.second-button.svelte-1eyrl7y"));
            submit.click();
            WebElement check = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.error.svelte-1eyrl7y"));
            System.out.println(check.getText());
            Assertions.assertTrue(check.isDisplayed());
        } finally {
            webDriver.close();
        }
    }

    @Test
    void sendFile() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Hamida\\Desktop\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        try {
            webDriver.get("https://mail.ru/");
            webDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
            WebElement email = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.email-container.svelte-1eyrl7y > div.email-input-container.svelte-1eyrl7y > input"));
            email.sendKeys("hamida-ibatech");
            WebElement next = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.button.svelte-1eyrl7y"));
            next.click();
            WebElement password = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > div.password-input-container.svelte-1eyrl7y > input"));
            password.sendKeys("ibatech123");
            WebElement submit = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > button.second-button.svelte-1eyrl7y"));
            submit.click();
//            Thread.sleep(5000);
            WebElement addEmail = webDriver.findElement(By.cssSelector("#app-canvas > div > div.application-mail > div.application-mail__overlay > div > div.application-mail__layout.application-mail__layout_main > span > div.layout__column.layout__column_left > div.layout__column-wrapper > div > div > div > div:nth-child(1) > div > div > a > span > span > svg"));
            addEmail.click();
//            Thread.sleep(5000);
            WebElement toWhom = webDriver.findElement(By.cssSelector("body > div.compose-windows.compose-windows_rise > div.compose-app.compose-app_fix.compose-app_popup.compose-app_window.compose-app_fixed > div > div.compose-app__compose > div.container--rp3CE.compressed--2kH7H > div.scrollview--SiHhk.scrollview_main--3Vfg9.scrollview_mainCompressed--2IhQY > div.head_container--3W05z > div > div > div.wrap--2sfxq.compressed--23JBG > div > div.contacts--1ofjA > div > div > label > div > div > input"));
            toWhom.sendKeys("hamidaaliyeva@gmail.com");
            WebElement subject = webDriver.findElement(By.cssSelector("body > div.compose-windows.compose-windows_rise > div.compose-app.compose-app_fix.compose-app_popup.compose-app_window.compose-app_fixed > div > div.compose-app__compose > div.container--rp3CE.compressed--2kH7H > div.scrollview--SiHhk.scrollview_main--3Vfg9.scrollview_mainCompressed--2IhQY > div.subject__container--HWnat > div.subject__wrapper--2mk6m > div.container--3QXHv.compressed--2KOQl > div > input"));
            subject.sendKeys("Homework1");
            WebElement shareFile = webDriver.findElement(By.cssSelector("body > div.compose-windows.compose-windows_rise > div.compose-app.compose-app_fix.compose-app_popup.compose-app_window.compose-app_fixed > div > div.compose-app__compose > div.container--rp3CE.compressed--2kH7H > div.scrollview--SiHhk.scrollview_main--3Vfg9.scrollview_mainCompressed--2IhQY > div.attach_container--2NlSY > div > div > div > button:nth-child(2) > input"));
            shareFile.sendKeys("C:\\Users\\Hamida\\Desktop\\Hw1.docx");
            WebElement send = webDriver.findElement(By.cssSelector("body > div.compose-windows.compose-windows_rise > div.compose-app.compose-app_fix.compose-app_popup.compose-app_window.compose-app_fixed > div > div.compose-app__footer > div.compose-app__buttons > span.button2.button2_base.button2_primary.button2_compact.button2_hover-support.js-shortcut > span"));
            send.click();
        } finally {
            webDriver.close();
        }
    }

    @Test
    void calculateAllGoodsWithDiscount() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Hamida\\Desktop\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        try {
            webDriver.get("https://www.etsy.com/ ");
            Actions builder = new Actions(webDriver);
            webDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
//            WebElement test = webDriver.findElement(By.cssSelector("#catnav-l4-11109"));
            WebElement clothingAndShoes = webDriver.findElement(By.cssSelector("#desktop-category-nav > div.wt-bg-white.wt-hide-xs.wt-show-lg.wt-text-caption.wt-position-relative.wt-z-index-4.v2-toolkit-cat-nav-tab-bar > div > ul > li:nth-child(2) > a"));
            builder.moveToElement(clothingAndShoes).build().perform();
            WebElement men = webDriver.findElement(By.cssSelector("#side-nav-category-link-10936"));
            men.click();
            WebElement boots = webDriver.findElement(By.cssSelector("#catnav-l4-11109"));
            boots.click();
            WebElement onSale = webDriver.findElement(By.cssSelector("#search-filter-reset-form > div:nth-child(2) > fieldset > div > div > div:nth-child(2) > div > a > label"));
            onSale.click();
            WebElement check = webDriver.findElement(By.cssSelector("#content > div > div.content.bg-white.col-md-12.pl-xs-1.pr-xs-0.pr-md-1.pl-lg-0.pr-lg-0.wt-bb-xs-1 > div > div.mt-xs-2.pl-xs-1.pl-md-4.pl-lg-6.pr-xs-1.pr-md-4.pr-lg-6 > div > span > span > span:nth-child(2)"));
            Assertions.assertTrue(check.isDisplayed());
        } finally {
            webDriver.close();
        }
    }

    @Test
    void calculateLinkNumbers() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Hamida\\Desktop\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        try {
            webDriver.get("https://www.etsy.com/ ");
            Actions builder = new Actions(webDriver);
            webDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
//            WebElement test = webDriver.findElement(By.cssSelector("#catnav-l4-11109"));
            WebElement clothingAndShoes = webDriver.findElement(By.cssSelector("#desktop-category-nav > div.wt-bg-white.wt-hide-xs.wt-show-lg.wt-text-caption.wt-position-relative.wt-z-index-4.v2-toolkit-cat-nav-tab-bar > div > ul > li:nth-child(2) > a"));
            builder.moveToElement(clothingAndShoes).build().perform();
            WebElement men = webDriver.findElement(By.cssSelector("#side-nav-category-link-10936"));
            men.click();
            List<WebElement> webElements = webDriver.findElements(By.cssSelector("#desktop-category-nav > div.wt-position-relative.wt-body-max-width > div > div:nth-child(2) > div > div > div > section:nth-child(2) > div > ul:nth-child(2) > li.wt-grid__item-xs-12.wt-pl-xs-0.wt-text-title-01.wt-pb-xs-1 > ul"));
            System.out.println(webElements.size());
        } finally {
            webDriver.close();
        }
    }
}
