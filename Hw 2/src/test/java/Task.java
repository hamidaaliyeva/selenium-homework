import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

public class Task {
    @Test
    void tasks() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Hamida\\Desktop\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        try {
            webDriver.get("https://carry.az");
            webDriver.findElement(By.xpath("//ul[@class='d-flex align-items-center menu']/li[5]")).click();
            JavascriptExecutor js = (JavascriptExecutor) webDriver;
            js.executeScript("window.scrollBy(0,600)");
            webDriver.findElement(By.xpath("//p[@class='text-justify aboutext mt-3']/a")).click();
            String parentWindow = webDriver.getWindowHandle();
            for (String windows : webDriver.getWindowHandles()
            ) {
                webDriver.switchTo().window(windows);
            }
            webDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
            System.out.println(webDriver.getCurrentUrl());
            Assertions.assertEquals("https://e.customs.gov.az/", webDriver.getCurrentUrl());
            webDriver.switchTo().window(parentWindow);
            webDriver.navigate().back();
            webDriver.findElement(By.id("ceki")).sendKeys("200");
            Select unit = new Select(webDriver.findElement(By.cssSelector("#unit")));
            unit.selectByVisibleText("Qram");
            webDriver.findElement(By.className("calc")).click();
            Assertions.assertTrue(webDriver.findElement(By.className("netice")).isDisplayed());
        } finally {
            webDriver.quit();
        }
    }

    @Test
    void testAlert() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Hamida\\Desktop\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        try {
            webDriver.get("https://rahulshettyacademy.com/AutomationPractice/");
            webDriver.findElement(By.id("name")).sendKeys("Hamida");
            Actions actions = new Actions(webDriver);
            actions.moveToElement(webDriver.findElement(By.id("name"))).doubleClick().build().perform();
            webDriver.findElement(By.id("confirmbtn")).click();
            webDriver.switchTo().alert().accept();
            Assertions.assertEquals("https://rahulshettyacademy.com/AutomationPractice/", webDriver.getCurrentUrl());
        } finally {
            webDriver.quit();
        }
    }

    @Test
    void testUploading() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Hamida\\Desktop\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        try {
            webDriver.get("http://demo.guru99.com/test/upload/");
            webDriver.findElement(By.id("uploadfile_0")).sendKeys("C:\\Users\\Hamida\\Downloads\\test.jpg");
            webDriver.findElement(By.id("terms")).click();
            webDriver.findElement(By.id("submitbutton")).click();
            Assertions.assertEquals("http://demo.guru99.com/test/upload/", webDriver.getCurrentUrl());
        } finally {
            webDriver.close();
        }
    }

    @Test
    void fileDownload() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Hamida\\Desktop\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        try {
            webDriver.get("https://the-internet.herokuapp.com");
            webDriver.findElement(By.linkText("File Download")).click();
            webDriver.findElement(By.linkText("hello_world.txt")).click();
            Assertions.assertEquals("https://the-internet.herokuapp.com/download", webDriver.getCurrentUrl());
        } finally {
            webDriver.close();
        }
    }

    @Test
    void testIframe() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Hamida\\Desktop\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        try {
            webDriver.get("http://demo.guru99.com/test/guru99home/");
            webDriver.switchTo().frame(webDriver.findElement(By.id("a077aa5e")));
            webDriver.findElement(By.cssSelector("body > a")).click();
            for (String windows : webDriver.getWindowHandles()
            ) {
                webDriver.switchTo().window(windows);
            }
            Assertions.assertEquals("https://www.guru99.com/live-selenium-project.html", webDriver.getCurrentUrl());
        } finally {
            webDriver.quit();
        }
    }
}
