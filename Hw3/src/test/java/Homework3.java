import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

public class Homework3 {

    @Test
    void task1() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Hamida\\Desktop\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        try {
            webDriver.get("https://www.globalsqa.com/demo-site/draganddrop/");
            Actions actions = new Actions(webDriver);
            webDriver.switchTo().frame(webDriver.findElement(By.xpath("//iframe[@class='demo-frame lazyloaded']")));
            WebElement source = webDriver.findElement(By.xpath("//ul[@id='gallery']/li[1]"));
            WebElement target = webDriver.findElement(By.xpath("//div[@id='trash']"));
            actions.dragAndDrop(source, target).perform();
            Assertions.assertTrue(webDriver.findElement(By.xpath("//div[@id='trash']/ul/li")).isDisplayed());
        } finally {
            webDriver.quit();
        }
    }

    @Test
    void task2() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Hamida\\Desktop\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        try {
            int expected = 249;
            webDriver.get("https://www.globalsqa.com/demo-site/select-dropdown-menu/");
            Select countries = new Select(webDriver.findElement(By.xpath("//select")));
            int actual = countries.getOptions().size();
            Assertions.assertTrue(actual == expected);
        } finally {
            webDriver.quit();
        }
    }

    @Test
    void task3() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Hamida\\Desktop\\chromedriver_win32\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        try {
            webDriver.get("https://www.globalsqa.com/samplepagetest/");
            webDriver.findElement(By.xpath("//input[@name='file-553']")).sendKeys("C:\\Users\\Hamida\\Downloads\\test.jpg");
            webDriver.findElement(By.id("g2599-name")).sendKeys("Hamida");
            webDriver.findElement(By.id("g2599-email")).sendKeys("hamidaaliyevaa@gmail.com");
            Select experience = new Select(webDriver.findElement(By.id("g2599-experienceinyears")));
            experience.selectByVisibleText("3-5");
            webDriver.findElement(By.xpath("//label[@class='grunion-checkbox-multiple-label checkbox-multiple'][1]")).click();
            webDriver.findElement(By.xpath("//label[@class='grunion-radio-label radio'][2]")).click();
            webDriver.findElement(By.id("contact-form-comment-g2599-comment")).sendKeys("comment");
            webDriver.findElement(By.xpath("//button[@type='submit']")).click();
            Assertions.assertEquals(webDriver.findElement(By.xpath("//blockquote[@class='contact-form-submission']/p[1]")).getText(), "Name: Hamida");
            Assertions.assertEquals(webDriver.findElement(By.xpath("//blockquote[@class='contact-form-submission']/p[2]")).getText(), "Email: hamidaaliyevaa@gmail.com");
            Assertions.assertEquals(webDriver.findElement(By.xpath("//blockquote[@class='contact-form-submission']/p[3]")).getText(), "Website:");
            Assertions.assertEquals(webDriver.findElement(By.xpath("//blockquote[@class='contact-form-submission']/p[4]")).getText(), "Experience (In Years): 3-5");
            Assertions.assertEquals(webDriver.findElement(By.xpath("//blockquote[@class='contact-form-submission']/p[5]")).getText(), "Expertise :: Functional Testing");
            Assertions.assertEquals(webDriver.findElement(By.xpath("//blockquote[@class='contact-form-submission']/p[6]")).getText(), "Education: Post Graduate");
            Assertions.assertEquals(webDriver.findElement(By.xpath("//blockquote[@class='contact-form-submission']/p[7]")).getText(), "Comment: comment");
        } finally {
            webDriver.quit();
        }
    }
}
